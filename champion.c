#include "champion.h"
#include <stdio.h>
#include <stdlib.h>


// dynamically Alloc a Champ with 25% for each role
// Mage, level is rand [5,8]
// Fighter, level is random [1,4]
// Support, level [3,6]
// Tank, level [1,6]
Champ* createChampion() {
  Champ* c = (Champ*)malloc(1 * sizeof(Champ));
  int clazz = rand() % 4;
  switch(clazz) {
    case 0:
      c->level = rand() % (8-5+1) + 5;
      break;
    case 1:
      c->level = rand() % (4-1+1) + 1;
      break;
    case 2:
      c->level = rand() % (6-3+1) + 3;
      break;
    case 3:
      c->level = rand() % (6-1+1) + 1;
      break;
    default:
      printf("Invalid Champ generated %d\n", clazz);
      exit(-1);
  }
  c->next=NULL;
  c->role=clazz;
  return c;
}


void _insertafter(Champ* curr, Champ* new) {
  new->next=curr->next;
  curr->next = new;
}

// insert sorted based on level, decreasing
Champ* addChamp(Champ *head, Champ *new) {
  if (new->level > head->level) {
    new->next = head;
    return new;
  }
  Champ *curr = head;
  // TODO this doesn't check the last item in the list for sortin
  // should be fine, because it it was alreaday checking in the while loop.
  for (;curr->next != NULL; curr = curr->next) { 
    if (curr->level >= new->level && new->level >= curr->next->level) {
      _insertafter(curr, new);
      break;
    }
  }
  if (curr->next == NULL) curr->next = new;
  return head;
}

// Creates a linked list of n generated champs
// must use create* and add*
// return head
Champ* buildChampList(int n) {
  Champ *head = createChampion();
  n--;
  for (;n > 0; n--) {
    //printf("debug: WERE LOOOOOPN\n");
    head = addChamp(head, createChampion());
  }
  return head;
}

// Prints linked list: S5 T3 M7
void printChampionList(Champ *head) {
  while (head != NULL) {
    switch(head->role) {
      case 0:
        printf("M");
        break;
      case 1:
        printf("F");
        break;
      case 2:
        printf("S");
        break;
      case 3:
        printf("T");
        break;
      default:
        printf("Invalid Champ printing %d", head->level);
        exit(-1);
    }
    printf("%d ", head->level);
    head = head->next;
  }
  printf("\n");
}

// removes and dealloc head returns new head
Champ* removeChamp(Champ *head) {
  Champ* new = head->next;
  free(head);
  return new;
}

//destructor for linked list returns null
Champ* destroyChampList(Champ *head) {
  while (head != NULL) {
    head = removeChamp(head);
  }
  return head;
}

