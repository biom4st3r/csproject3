#ifndef CHAMPH
#define CHAMPH

typedef enum {
  M,
  F,
  S,
  T,
} ChampRole;

typedef struct Champ {
  ChampRole role;
  int level;
  struct Champ *next;
} Champ;

// dynamically Alloc a Champ with 25% for each role
// Mage, level is rand [5,8]
// Fighter, level is random [1,4]
// Support, level [3,6]
// Tank, level [1,6]
Champ* createChampion();

// insert sorted based on level, decreasing
Champ* addChamp(Champ *head, Champ *c);

// Creates a linked list of n generated champs
// must use create* and add*
// return head
Champ* buildChampList(int n);

// Prints linked list: S5 T3 M7
void printChampionList(Champ *head);

// removes and dealloc head returns new head
Champ* removeChamp(Champ *head);

//destructor for linked list returns null
Champ* destroyChampList(Champ *head);
#endif



