#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"
#define _ARG_SIZE 5


int startswith(char* target, char* pattern) {
  if (strlen(target) <= strlen(pattern)) return 0; // impossible
  return strncmp(target, pattern, strlen(pattern)) == 0;
}

void example_usage(char** argv) {
  printf("Example usage:\n%s ARMY_SIZE [--p1=\"Player1Name\" --p2=\"Player2Name\" --seed=1337]\n", argv[0]);
}

void _trim_arg(char* arg) {
      int j = 0;
      // -1 to ensure null byte is copied
      for (;arg[j+_ARG_SIZE-1] != 0;j++) {
        // remove the first 5 bytes of the string
        arg[j] = arg[j+_ARG_SIZE];
      }
}

void parse(int count, char** argv, char** p1name, char** p2name, int* seed) {
  if (count < 2) {
    return;
  }
  // look for args after the project
  // project required argument for army size
  for (int i = 2; i < count; i++) {
    if (startswith(argv[i], "--p1=")) {
      _trim_arg(argv[i]);
      *p1name=argv[i];
      //sscanf(argv[i], "--p1=%s", p1name);
    } else if (startswith(argv[i], "--p2=")) {
      _trim_arg(argv[i]);
      *p2name=argv[i];
    } else if (startswith(argv[i], "--seed=")) {
      int result = sscanf(argv[i], "--seed=%d", seed);
      //printf("seed sscanf: %d __ %d\n", result, result == 0);
      if (result == 0) {
        printf("Invalid seed: %s. Please use numbers only\n", argv[i]);
        example_usage(argv);
        exit(-1);
      }
    } else {
      printf("unknown argument: %s\n", argv[i]);
      example_usage(argv);
      exit(-1);
    }
  }
}
