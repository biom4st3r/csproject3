#ifndef PARSE_H
#define PARSE_H

void example_usage(char** argv);
void parse(int count, char** argv, char** p1name, char** p2name, int* seed);
#endif
