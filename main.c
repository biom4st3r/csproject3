#include "champion.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "parser.h"
#define true 1
#define false 0
#define bool int

char ENUM2STR[4][8] = {"MAGE","FIGHTER","SUPPORT","TANK"};
bool winner(Champ *p1, Champ *p2, char*,char*);
void _post_round_flavor(int[],char*,char*);
int abs(int i) {
  return (i > 0) ? i : i*-1;
}

int main(int count, char **args) {
  char* p1name=NULL;
  char* p2name=NULL;
  int seed = -1;
  // passes a reference to the *name to change 
  // the pointer instead of values, because
  // I don't allocate for them. They'll end up
  // pointer somewhere in args
  parse(count, args, &p1name, &p2name, &seed);
  if (seed == -1) srand(time(NULL));
  else srand(seed);
  if (p1name == NULL) p1name = "Player 1";
  if (p2name == NULL) p2name = "Player 2";
  
  if (count < 2) {
    example_usage(args);
    //printf("Incorrect args\n Example usage:\n./project3 {#}\n");
    return -1;
  }

  int armysize = 0;
  if (sscanf(args[1], "%d", &armysize) != 1 || armysize < 1) {
    printf("Invalid argument for ARMY_SIZE \"%s\"\nmust be number > 0\n", args[1]);
    example_usage(args);
    return -1;
  }
  // 1 arg: starting chars
  // if no: print error and exit
  // if <=0: print erro and exit
  //printf("debug: init player1\n");
  Champ *player1 = buildChampList(armysize);
  //printf("debug: init player2\n");
  Champ *player2 = buildChampList(armysize);
  // build both players army
  // start game loop
  int round = 0;
  int result[2];
  for (;; round++) {
    result[0] = 0;
    result[1] = 0;
    
    bool p1hl = player1->level > player2->level;
    bool p2hl = player2->level > player1->level;
    //bool samel = !p1hl && !p2hl;
    printf("\n--Round %d--\n", round+1);
    printf("%s: ", p1name);
    printChampionList(player1);
    printf("%s: ", p2name);
    printChampionList(player2);
    int encounter = (player1->role==M||player2->role==M ? 0b1000 : 0b0) |
      (player1->role==F||player2->role==F ? 0b100 : 0b0) |
      (player1->role==S||player2->role==S ? 0b10 : 0b0) |
      (player1->role==T||player2->role==T ? 0b1 : 0b0);
    switch(encounter) {
      case 0b1000: // MM
        if (p1hl) {
          result[0] = 1;
          result[1] = -1;
        } else if (p2hl) {
          result[0] = -1;
          result[1] = 1;
        }
        break;
      case 0b1100: // MF
        if (player1->role==M) {
          if (p1hl) {
            result[0] = 1;
            result[1] = -1;
          } else if (p2hl) {
            result[0] = -1;
            result[1] = 0;
          }
        } else {
          if (p1hl) { // F win
            result[0] = 0;
            result[1] = -1;
          } else {
            result[0] = -1;
            result[1] = 1;
          }
        }
        break;
      case 0b1010: // MS
        if (player1->role==M) {
          if (p1hl) {
            result[0] = 1;
            result[1] = -2;
          } else if (p2hl) {
            result[0] = -1;
            result[1] = 2;
          }
        } else {
          if (p1hl) {
            result[0] = 2;
            result[1] = -1;
          } else if(p2hl) {
            result[0] = -2;
            result[1] = 1;
          } 
        }
        break;
      case 0b1001: // MT
        if (player1->role==M) {
          result[0] = 1;
          result[1] = -1;
        } else {
          result[0] = -1;
          result[1] = 1;
        }
        break;
      case 0b0100: // FF
        result[0] = 1;
        result[1] = 1;
        break;
      case 0b0110: // FS
        if (player1->role==F) {
          if (p2hl) {
            result[0] = 0;
            result[1] = 1;
          } else {
            result[0] = 0;
            result[1] = -1;
          }
        } else {
          if (p2hl) {
            result[0] = -1;
            result[1] = 0;
          } else {
            result[0] = 1;
            result[1] = 0;
          }
        }
        break;
      case 0b0101: // FT
        if (player1->role==F) {
          result[0] = 1;
          result[1] = 0;
        } else {
          result[0] = 0;
          result[1] = 1;
        }
        break;
      case 0b0010: // SS
        result[0] = -1;
        result[1] = -1;
        break;
      case 0b0011: // ST
        if (player1->role==S) {
          result[0] = 0;
          result[1] = 1;
        } else {
          result[0] = 1;
          result[1] = 0;
        }
        break;
      case 0b0001: // TT
        // pass
        break;
      default:
        printf("...? T_T\n");
    }

    printf("%s is %s | %s is %s\n", p1name, ENUM2STR[player1->role], p2name, ENUM2STR[player2->role]);
    _post_round_flavor(result, p1name, p2name);
    //printf("debug: removing for this round\n");
    player1 = removeChamp(player1);
    player2 = removeChamp(player2);
    //printf("debug: checking winners\n");

    if (winner(player1,player2, p1name, p2name)) return 0;
    
    //printf("debug: applying results p1p\n");
    for (;result[0] > 0; result[0] = result[0] - 1) {
      //printf("gift player1, ");
      player1 = addChamp(player1, createChampion());
    }

    //printf("debug: applying results p1n\n");
    for (;result[0] < 0; result[0] = result[0] + 1) {
      player1 = removeChamp(player1);
      if (winner(player1, player2, p1name, p2name)) return 0;
      //printf("hurt player1, ");
    }

    //printf("debug: applying results p2p\n");
    for (;result[1] > 0; result[1] = result[1] - 1) {
      player2 = addChamp(player2, createChampion());
      //printf("gift player2, ");
    }

    //printf("debug: applying results p2n\n");
    for (;result[1] < 0; result[1] = result[1] + 1) {
      player2 = removeChamp(player2);
      if (winner(player1, player2, p1name, p2name)) return 0;
      //printf("hurt player2, ");
    }

    //printf("debug: checking winners\n");
    if (winner(player1,player2,p1name,p2name)) return 0;
  }
  // print round number starting at 1
  // print each player list
  // battle
  // remove active char before reward/punish

  return 0;
}

void _tie_text() {
  int i = rand() % 10;
  switch(i) {
    case 8:
      printf("Everyone took the day off for Robonica.\n");
      break;
    case 9:
      printf("The Champions gave up for the day due to rain.\n");
      break;
    default:
      printf("No casualties sustained.\n");
      break;
  }
}

void _post_round_flavor(int* result, char* p1name, char* p2name) {
  if (abs(result[0]) + abs(result[1]) == 0) {
    _tie_text();
    return;
  }
  if (result[0] == 0) {
    printf("%s is unscathed!\n", p1name);
  } else 
    printf("%s %s %d Champions!\n", p1name, (result[0] > 0 ? "gains" : "loses"), abs(result[0]));
  if (result[1] == 0) {
    printf("%s is unscathed!\n", p2name);
  } else
    printf("%s %s %d Champions!\n", p2name, (result[1] > 0 ? "gains" : "loses"), abs(result[1]));
}

bool winner(Champ *p1, Champ *p2, char* p1name, char* p2name) {
  if (p1 == NULL) {
    printf("%s Wins!\n", p2name);
    destroyChampList(p2);
    return true;
  } else if (p2 == NULL) {
    printf("%s Wins!\n", p1name);
    destroyChampList(p1);
    return true;
  }
  return false;
}
