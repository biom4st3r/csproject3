Project3Exe : main.o champion.o parser.o
	gcc main.o champion.o parser.o -o Project3Exe

main.o : main.c champion.h parser.h
	gcc -Wall -c main.c

champion.o : champion.c champion.h
	gcc -Wall -c champion.c

parser.o : parser.c parser.h
	gcc -Wall -c parser.c

clean :
	rm *.o Project3Exe